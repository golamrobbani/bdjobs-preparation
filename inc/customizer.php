<?php
/**
 * bdjobspreparation Theme Customizer
 *
 * @package bdjobspreparation
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function bdjobspreparation_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

}
add_action( 'customize_register', 'bdjobspreparation_customize_register' );

function bdjobspreparation_theme_customize_register( $wp_customize ) {

    $wp_customize->add_section( 'bdjobspreparation_theme_slider_options', array(
        'title'          => __( 'Slider Settings', 'bdjobspreparation' )
    ) );

    $wp_customize->add_setting( 'bdjobspreparation_theme_slider_count_setting', array(
        'default'        => '1',
        'sanitize_callback' => 'absint'
    ) );

    $wp_customize->add_control( 'bdjobspreparation_theme_slider_count', array(
        'label'      => __( 'Number of slides displaying at once', 'bdjobspreparation' ),
        'section'    => 'bdjobspreparation_theme_slider_options',
        'type'       => 'text',
        'settings'   => 'bdjobspreparation_theme_slider_count_setting'
    ) );

    $wp_customize->add_setting( 'bdjobspreparation_theme_slider_time_setting', array(
        'default'        => '5000',
        'sanitize_callback' => 'absint'
    ) );

    $wp_customize->add_control( 'bdjobspreparation_theme_slider_time', array(
        'label'      => __( 'Slider Time (in ms)', 'bdjobspreparation' ),
        'section'    => 'bdjobspreparation_theme_slider_options',
        'type'       => 'text',
        'settings'   => 'bdjobspreparation_theme_slider_time_setting'
    ) );

    $wp_customize->add_control( 'logo_url', array(
        'label'      => __( 'Logo URL', 'bdjobspreparation' ),
        'section'    => 'bdjobspreparation_theme_slider_options',
        'type'       => 'url',
        'settings'   => 'bdjobspreparation_theme_slider_time_setting'
    ) );
}
add_action( 'customize_register', 'bdjobspreparation_theme_customize_register' );



/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function bdjobspreparation_customize_preview_js() {
	wp_enqueue_script( 'bdjobspreparation_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'bdjobspreparation_customize_preview_js' );
